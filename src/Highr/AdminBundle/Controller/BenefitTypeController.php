<?php

namespace Highr\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Highr\HighrBundle\Entity\BenefitType;
use Highr\HighrBundle\Form\BenefitTypeType;

/**
 * BenefitType controller.
 *
 * @Route("/admin/benefittype")
 */
class BenefitTypeController extends Controller
{

    /**
     * Lists all BenefitType entities.
     *
     * @Route("/", name="benefittype")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('HighrBundle:BenefitType')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new BenefitType entity.
     *
     * @Route("/", name="benefittype_create")
     * @Method("POST")
     * @Template("HighrBundle:BenefitType:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new BenefitType();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('benefittype_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a BenefitType entity.
     *
     * @param BenefitType $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(BenefitType $entity)
    {
        $form = $this->createForm(new BenefitTypeType(), $entity, array(
            'action' => $this->generateUrl('benefittype_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new BenefitType entity.
     *
     * @Route("/new", name="benefittype_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new BenefitType();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a BenefitType entity.
     *
     * @Route("/{id}", name="benefittype_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HighrBundle:BenefitType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BenefitType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing BenefitType entity.
     *
     * @Route("/{id}/edit", name="benefittype_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HighrBundle:BenefitType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BenefitType entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a BenefitType entity.
    *
    * @param BenefitType $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(BenefitType $entity)
    {
        $form = $this->createForm(new BenefitTypeType(), $entity, array(
            'action' => $this->generateUrl('benefittype_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing BenefitType entity.
     *
     * @Route("/{id}", name="benefittype_update")
     * @Method("PUT")
     * @Template("HighrBundle:BenefitType:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HighrBundle:BenefitType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find BenefitType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('benefittype_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a BenefitType entity.
     *
     * @Route("/{id}", name="benefittype_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('HighrBundle:BenefitType')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find BenefitType entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('benefittype'));
    }

    /**
     * Creates a form to delete a BenefitType entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('benefittype_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
