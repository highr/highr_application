<?php

namespace Highr\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Highr\HighrBundle\Entity\Company;
use Highr\HighrBundle\Form\CompanyType;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Company controller.
 *
 * @Route("/admin/company")
 */
class CompanyController extends Controller
{

    /**
     * Lists all Company entities.
     *
     * @Route("/", name="company")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('HighrBundle:Company')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Company entity.
     *
     * @Route("/", name="company_create")
     * @Method("POST")
     * @Template("AdminBundle:Company:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $company = new Company();
        $form = $this->createCreateForm($company);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $company->setDisabled(true);

            if ( $company->getImage() ) {
                $image = $company->getImage();
                $uploadsDir = $this->container->getParameter('uploads_directory');

                $newImageFileName = time() . '_' . $image->getClientOriginalName();
                $newImagePath = $uploadsDir . DIRECTORY_SEPARATOR;
                $image->move($newImagePath, $newImageFileName);

                $company->setImage($newImageFileName);
            }

            if ( $company->getLogo() ) {
                $logo = $company->getLogo();
                $uploadsDir = $this->container->getParameter('uploads_directory');

                $newLogoFileName = time() . '_' . $logo->getClientOriginalName();
                $newLogoPath = $uploadsDir . DIRECTORY_SEPARATOR;
                $logo->move($newLogoPath, $newLogoFileName);

                $company->setLogo($newLogoFileName);
            }

            $em->persist($company);
            $em->flush();

            return $this->redirect($this->generateUrl('company_show', array('id' => $company->getId())));
        }

        return array(
            'entity' => $company,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a Company entity.
     *
     * @param Company $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Company $entity)
    {
        $form = $this->createForm(new CompanyType(), $entity, array(
            'action' => $this->generateUrl('company_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Company entity.
     *
     * @Route("/new", name="company_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new Company();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a Company entity.
     *
     * @Route("/{id}", name="company_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HighrBundle:Company')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Company entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'company'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Company entity.
     *
     * @Route("/{id}/edit", name="company_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $company = $em->getRepository('HighrBundle:Company')->find($id);

        if (!$company) {
            throw $this->createNotFoundException('Unable to find Company entity.');
        }

        $editForm = $this->createEditForm($company);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'company'      => $company,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a Company entity.
    *
    * @param Company $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Company $entity)
    {
        $form = $this->createForm(new CompanyType(), $entity, array(
            'action' => $this->generateUrl('company_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Company entity.
     *
     * @Route("/{id}", name="company_update")
     * @Method("PUT")
     * @Template("AdminBundle:Company:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $company = $em->getRepository('HighrBundle:Company')->find($id);

        if (!$company) {
            throw $this->createNotFoundException('Unable to find Company entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($company);
        $editForm->handleRequest($request);


        if ($editForm->isValid()) {
            $this->handleUploadedFiles($company);

            $em->flush();

            return $this->redirect($this->generateUrl('company_edit', array('id' => $id)));
        }

        return array(
            'company'      => $company,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a Company entity.
     *
     * @Route("/{id}", name="company_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('HighrBundle:Company')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Company entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('company'));
    }

    /**
     * Creates a form to delete a Company entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('company_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }

    /**
     * @param Company $company
     */
    protected function handleUploadedFiles($company) {
        if ( $company->getImage() ) {
            $image = $company->getImage();
            $uploadsDir = $this->container->getParameter('uploads_directory');
            $newImageFileName = time() . '_' . $image->getClientOriginalName();
            $newImagePath = $uploadsDir . DIRECTORY_SEPARATOR;
            $image->move($newImagePath, $newImageFileName);

            $company->setImage($newImageFileName);
        }

        if ( $company->getLogo() ) {
            $logo = $company->getLogo();
            $uploadsDir = $this->container->getParameter('uploads_directory');

            $newLogoFileName = time() . '_' . $logo->getClientOriginalName();
            $newLogoPath = $uploadsDir . DIRECTORY_SEPARATOR;
            $logo->move($newLogoPath, $newLogoFileName);

            $company->setLogo($newLogoFileName);
        }
    }
}
