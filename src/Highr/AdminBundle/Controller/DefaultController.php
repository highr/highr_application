<?php

namespace Highr\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;

/**
 * Class DefaultController
 * @package Highr\AdminBundle\Controller
 * @Route("/admin")
 */
class DefaultController extends Controller
{
    /**
     * @Route("/admin", name="admin_dashboard")
     * @Method("GET")
     * @Template()
     */
    public function indexAction() {

        return array('name' => 'test');
    }


    /**
     * @Route("/admin/userlist", name="admin_userlist")
     * @Method("GET")
     * @Template()
     */
    public function  userListAction(){

        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('HighrBundle:User')->findAll();

        return array(
            'entities' => $entities,
        );
    }
}
