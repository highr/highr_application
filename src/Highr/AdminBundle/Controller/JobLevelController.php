<?php

namespace Highr\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Highr\HighrBundle\Entity\JobLevel;
use Highr\HighrBundle\Form\JobLevelType;

/**
 * JobLevel controller.
 *
 * @Route("/admin/joblevel")
 */
class JobLevelController extends Controller
{

    /**
     * Lists all JobLevel entities.
     *
     * @Route("/", name="joblevel")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('HighrBundle:JobLevel')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new JobLevel entity.
     *
     * @Route("/", name="joblevel_create")
     * @Method("POST")
     * @Template("HighrBundle:JobLevel:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new JobLevel();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('joblevel_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a JobLevel entity.
     *
     * @param JobLevel $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(JobLevel $entity)
    {
        $form = $this->createForm(new JobLevelType(), $entity, array(
            'action' => $this->generateUrl('joblevel_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new JobLevel entity.
     *
     * @Route("/new", name="joblevel_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new JobLevel();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a JobLevel entity.
     *
     * @Route("/{id}", name="joblevel_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HighrBundle:JobLevel')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find JobLevel entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing JobLevel entity.
     *
     * @Route("/{id}/edit", name="joblevel_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HighrBundle:JobLevel')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find JobLevel entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a JobLevel entity.
    *
    * @param JobLevel $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(JobLevel $entity)
    {
        $form = $this->createForm(new JobLevelType(), $entity, array(
            'action' => $this->generateUrl('joblevel_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing JobLevel entity.
     *
     * @Route("/{id}", name="joblevel_update")
     * @Method("PUT")
     * @Template("HighrBundle:JobLevel:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HighrBundle:JobLevel')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find JobLevel entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('joblevel_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a JobLevel entity.
     *
     * @Route("/{id}", name="joblevel_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('HighrBundle:JobLevel')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find JobLevel entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('joblevel'));
    }

    /**
     * Creates a form to delete a JobLevel entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('joblevel_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
