<?php

namespace Highr\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Highr\HighrBundle\Entity\SkillGroup;
use Highr\HighrBundle\Form\SkillGroupType;

/**
 * SkillGroup controller.
 *
 * @Route("/admin/skillgroup")
 */
class SkillGroupController extends Controller
{

    /**
     * Lists all SkillGroup entities.
     *
     * @Route("", name="skillgroup")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('HighrBundle:SkillGroup')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new SkillGroup entity.
     *
     * @Route("/", name="skillgroup_create")
     * @Method("POST")
     * @Template("HighrBundle:SkillGroup:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new SkillGroup();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('skillgroup_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a SkillGroup entity.
     *
     * @param SkillGroup $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SkillGroup $entity)
    {
        $form = $this->createForm(new SkillGroupType(), $entity, array(
            'action' => $this->generateUrl('skillgroup_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SkillGroup entity.
     *
     * @Route("/new", name="skillgroup_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new SkillGroup();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a SkillGroup entity.
     *
     * @Route("/{id}", name="skillgroup_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HighrBundle:SkillGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SkillGroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing SkillGroup entity.
     *
     * @Route("/{id}/edit", name="skillgroup_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HighrBundle:SkillGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SkillGroup entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a SkillGroup entity.
    *
    * @param SkillGroup $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SkillGroup $entity)
    {
        $form = $this->createForm(new SkillGroupType(), $entity, array(
            'action' => $this->generateUrl('skillgroup_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing SkillGroup entity.
     *
     * @Route("/{id}", name="skillgroup_update")
     * @Method("PUT")
     * @Template("HighrBundle:SkillGroup:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HighrBundle:SkillGroup')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SkillGroup entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('skillgroup_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a SkillGroup entity.
     *
     * @Route("/{id}", name="skillgroup_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('HighrBundle:SkillGroup')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SkillGroup entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('skillgroup'));
    }

    /**
     * Creates a form to delete a SkillGroup entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('skillgroup_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
