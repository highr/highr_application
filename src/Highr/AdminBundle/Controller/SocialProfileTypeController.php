<?php

namespace Highr\AdminBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Highr\HighrBundle\Entity\SocialProfileType;
use Highr\HighrBundle\Form\SocialProfileTypeType;

/**
 * SocialProfileType controller.
 *
 * @Route("/admin/socialprofiletype")
 */
class SocialProfileTypeController extends Controller
{

    /**
     * Lists all SocialProfileType entities.
     *
     * @Route("/", name="socialprofiletype")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('HighrBundle:SocialProfileType')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new SocialProfileType entity.
     *
     * @Route("/", name="socialprofiletype_create")
     * @Method("POST")
     * @Template("HighrBundle:SocialProfileType:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new SocialProfileType();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('socialprofiletype_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a SocialProfileType entity.
     *
     * @param SocialProfileType $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(SocialProfileType $entity)
    {
        $form = $this->createForm(new SocialProfileTypeType(), $entity, array(
            'action' => $this->generateUrl('socialprofiletype_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new SocialProfileType entity.
     *
     * @Route("/new", name="socialprofiletype_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new SocialProfileType();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a SocialProfileType entity.
     *
     * @Route("/{id}", name="socialprofiletype_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HighrBundle:SocialProfileType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SocialProfileType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing SocialProfileType entity.
     *
     * @Route("/{id}/edit", name="socialprofiletype_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HighrBundle:SocialProfileType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SocialProfileType entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a SocialProfileType entity.
    *
    * @param SocialProfileType $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(SocialProfileType $entity)
    {
        $form = $this->createForm(new SocialProfileTypeType(), $entity, array(
            'action' => $this->generateUrl('socialprofiletype_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing SocialProfileType entity.
     *
     * @Route("/{id}", name="socialprofiletype_update")
     * @Method("PUT")
     * @Template("HighrBundle:SocialProfileType:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('HighrBundle:SocialProfileType')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find SocialProfileType entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('socialprofiletype_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a SocialProfileType entity.
     *
     * @Route("/{id}", name="socialprofiletype_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('HighrBundle:SocialProfileType')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find SocialProfileType entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('socialprofiletype'));
    }

    /**
     * Creates a form to delete a SocialProfileType entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('socialprofiletype_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
