var $collectionHolder;

jQuery(document).ready(function() {
    var benefitCollectionHolder = $('.benefitCollection');
    var socialProfileCollectionHolder = $('.socialProfilesCollection');
    var companyAddressCollectionHolder = $('.companyAddressesCollection');

    $('.addBenefit').on('click', function(e) {
        e.preventDefault();

        var prototype = $('#highr_highrbundle_company_benefits').data('prototype');
        var index = benefitCollectionHolder.data('index');
        
        var newForm = prototype.replace(/__name__/g, index);
        
        benefitCollectionHolder.data('index', index+1);
        benefitCollectionHolder.append(newForm);
    });
    
    $('.addSocialProfile').on('click', function(e) {
        e.preventDefault();

        var prototype = $('#highr_highrbundle_company_socialProfiles').data('prototype');
        var index = socialProfileCollectionHolder.data('index');
        
        var newForm = prototype.replace(/__name__/g, index);
        
        socialProfileCollectionHolder.data('index', index+1);
        socialProfileCollectionHolder.append(newForm);
    });
    
    $('.addCompanyAddress').on('click', function(e) {
        e.preventDefault();

        var prototype = $('#highr_highrbundle_company_companyAddresses').data('prototype');
        var index = companyAddressCollectionHolder.data('index');
        
        var newForm = prototype.replace(/__name__/g, index);
        
        companyAddressCollectionHolder.data('index', index+1);
        companyAddressCollectionHolder.append(newForm);
    });
});