<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Benefit
 *
 * @ORM\Table(name="benefit")
 * @ORM\Entity
 */
class Benefit
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string", length=255)
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity="BenefitType")
     * @ORM\JoinColumn(name="benefit_type_id", referencedColumnName="id")
     **/
    private $benefitType;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Benefit
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set benefitType
     *
     * @param \Highr\HighrBundle\Entity\BenefitType $benefitType
     * @return Benefit
     */
    public function setBenefitType(\Highr\HighrBundle\Entity\BenefitType $benefitType = null)
    {
        $this->benefitType = $benefitType;

        return $this;
    }

    /**
     * Get benefitType
     *
     * @return \Highr\HighrBundle\Entity\BenefitType 
     */
    public function getBenefitType()
    {
        return $this->benefitType;
    }
}
