<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Candidate
 *
 * @ORM\Table(name="candidate")
 * @ORM\Entity
 */
class Candidate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthday", type="datetime", nullable=true)
     */
    private $birthday;

    /**
     * @var string
     *
     * @ORM\Column(name="nationality", type="string", length=255, nullable=true)
     */
    private $nationality;

    /**
     * @var string
     *
     * @ORM\Column(name="about_me", type="text", nullable=true)
     */
    private $aboutMe;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var integer
     *
     * @ORM\Column(name="current_salary", type="integer", nullable=true)
     */
    private $currentSalary;

    /**
     * @var integer
     *
     * @ORM\Column(name="desired_salary", type="integer", nullable=true)
     */
    private $desiredSalary;

    /**
     * @var integer
     *
     * @ORM\Column(name="minimum_salary", type="integer", nullable=true)
     */
    private $minimumSalary;

    /**
     * @var string
     *
     * @ORM\Column(name="motivation", type="text", nullable=true)
     */
    private $motivation;

    /**
     * @var integer
     *
     * @ORM\Column(name="cancelation_period", type="integer", nullable=true)
     */
    private $cancelationPeriod;

    /**
     * @var integer
     *
     * @ORM\Column(name="desired_team_size", type="integer", nullable=true)
     */
    private $desiredTeamSize;

    /**
     * @var integer
     *
     * @ORM\Column(name="sex", type="integer", nullable=true)
     */
    private $sex;

    /**
     * @ORM\ManyToMany(targetEntity="Education")
     * @ORM\JoinTable(name="candidates_educations",
     *      joinColumns={@ORM\JoinColumn(name="candidate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="education_id", referencedColumnName="id", unique=true, nullable=true)}
     *      )
     **/
    private $educations;

    /**
     * @ORM\ManyToOne(targetEntity="JobLevel")
     * @ORM\JoinColumn(name="joblevel_id", referencedColumnName="id")
     **/
    private $desiredNextLevel;

    /**
     * @ORM\ManyToMany(targetEntity="BenefitType")
     * @ORM\JoinTable(name="candidates_benefittypes",
     *      joinColumns={@ORM\JoinColumn(name="candidate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="benefit_id", referencedColumnName="id", unique=true, nullable=true)}
     *      )
     **/
    private $benefitTypes;

    /**
     * @ORM\ManyToMany(targetEntity="Skill")
     * @ORM\JoinTable(name="candidates_skills",
     *      joinColumns={@ORM\JoinColumn(name="candidate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="skill_id", referencedColumnName="id", unique=true, nullable=true)}
     *      )
     **/
    private $skills;

    /**
     * @ORM\OneToOne(targetEntity="Address")
     * @ORM\JoinColumn(name="address_id", referencedColumnName="id")
     **/
    private $address;

    /**
     * @ORM\ManyToMany(targetEntity="PreviousJob")
     * @ORM\JoinTable(name="candidates_previousjobs",
     *      joinColumns={@ORM\JoinColumn(name="candidate_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="prevoiusjob_id", referencedColumnName="id", unique=true, nullable=true)}
     *      )
     **/
    private $previousJobs;

    /**
     * @ORM\ManyToOne(targetEntity="SocialProfile")
     * @ORM\JoinColumn(name="socialprofile_id", referencedColumnName="id", nullable=true)
     **/
    private $socialProfiles;

    public function __construct() {
        $this->educations = new ArrayCollection();
        $this->benefitTypes = new ArrayCollection();
        $this->skills = new ArrayCollection();
        $this->previousJobs = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set birthday
     *
     * @param \DateTime $birthday
     * @return Candidate
     */
    public function setBirthday($birthday)
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * Get birthday
     *
     * @return \DateTime 
     */
    public function getBirthday()
    {
        return $this->birthday;
    }

    /**
     * Set nationality
     *
     * @param string $nationality
     * @return Candidate
     */
    public function setNationality($nationality)
    {
        $this->nationality = $nationality;

        return $this;
    }

    /**
     * Get nationality
     *
     * @return string 
     */
    public function getNationality()
    {
        return $this->nationality;
    }

    /**
     * Set aboutMe
     *
     * @param string $aboutMe
     * @return Candidate
     */
    public function setAboutMe($aboutMe)
    {
        $this->aboutMe = $aboutMe;

        return $this;
    }

    /**
     * Get aboutMe
     *
     * @return string 
     */
    public function getAboutMe()
    {
        return $this->aboutMe;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Candidate
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set currentSalary
     *
     * @param integer $currentSalary
     * @return Candidate
     */
    public function setCurrentSalary($currentSalary)
    {
        $this->currentSalary = $currentSalary;

        return $this;
    }

    /**
     * Get currentSalary
     *
     * @return integer 
     */
    public function getCurrentSalary()
    {
        return $this->currentSalary;
    }

    /**
     * Set desiredSalary
     *
     * @param integer $desiredSalary
     * @return Candidate
     */
    public function setDesiredSalary($desiredSalary)
    {
        $this->desiredSalary = $desiredSalary;

        return $this;
    }

    /**
     * Get desiredSalary
     *
     * @return integer 
     */
    public function getDesiredSalary()
    {
        return $this->desiredSalary;
    }

    /**
     * Set minimumSalary
     *
     * @param integer $minimumSalary
     * @return Candidate
     */
    public function setMinimumSalary($minimumSalary)
    {
        $this->minimumSalary = $minimumSalary;

        return $this;
    }

    /**
     * Get minimumSalary
     *
     * @return integer 
     */
    public function getMinimumSalary()
    {
        return $this->minimumSalary;
    }

    /**
     * Set motivation
     *
     * @param string $motivation
     * @return Candidate
     */
    public function setMotivation($motivation)
    {
        $this->motivation = $motivation;

        return $this;
    }

    /**
     * Get motivation
     *
     * @return string 
     */
    public function getMotivation()
    {
        return $this->motivation;
    }

    /**
     * Set cancelationPeriod
     *
     * @param integer $cancelationPeriod
     * @return Candidate
     */
    public function setCancelationPeriod($cancelationPeriod)
    {
        $this->cancelationPeriod = $cancelationPeriod;

        return $this;
    }

    /**
     * Get cancelationPeriod
     *
     * @return integer 
     */
    public function getCancelationPeriod()
    {
        return $this->cancelationPeriod;
    }

    /**
     * Set desiredTeamSize
     *
     * @param integer $desiredTeamSize
     * @return Candidate
     */
    public function setDesiredTeamSize($desiredTeamSize)
    {
        $this->desiredTeamSize = $desiredTeamSize;

        return $this;
    }

    /**
     * Get desiredTeamSize
     *
     * @return integer 
     */
    public function getDesiredTeamSize()
    {
        return $this->desiredTeamSize;
    }

    /**
     * Set sex
     *
     * @param integer $sex
     * @return Candidate
     */
    public function setSex($sex)
    {
        $this->sex = $sex;

        return $this;
    }

    /**
     * Get sex
     *
     * @return integer 
     */
    public function getSex()
    {
        return $this->sex;
    }

    /**
     * Set desiredNextLevel
     *
     * @param JobLevel $desiredNextLevel
     * @return Candidate
     */
    public function setDesiredNextLevel($desiredNextLevel)
    {
        $this->desiredNextLevel = $desiredNextLevel;

        return $this;
    }

    /**
     * Get desiredNextLevel
     *
     * @return JobLevel
     */
    public function getDesiredNextLevel()
    {
        return $this->desiredNextLevel;
    }

    /**
     * Add educations
     *
     * @param \Highr\HighrBundle\Entity\Education $educations
     * @return Candidate
     */
    public function addEducation(\Highr\HighrBundle\Entity\Education $educations)
    {
        $this->educations[] = $educations;

        return $this;
    }

    /**
     * Remove educations
     *
     * @param \Highr\HighrBundle\Entity\Education $educations
     */
    public function removeEducation(\Highr\HighrBundle\Entity\Education $educations)
    {
        $this->educations->removeElement($educations);
    }

    /**
     * Get educations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEducations()
    {
        return $this->educations;
    }

    /**
     * Add benefitTypes
     *
     * @param \Highr\HighrBundle\Entity\BenefitType $benefitTypes
     * @return Candidate
     */
    public function addBenefitType(\Highr\HighrBundle\Entity\BenefitType $benefitTypes)
    {
        $this->benefitTypes[] = $benefitTypes;

        return $this;
    }

    /**
     * Remove benefitTypes
     *
     * @param \Highr\HighrBundle\Entity\BenefitType $benefitTypes
     */
    public function removeBenefitType(\Highr\HighrBundle\Entity\BenefitType $benefitTypes)
    {
        $this->benefitTypes->removeElement($benefitTypes);
    }

    /**
     * Get benefitTypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBenefitTypes()
    {
        return $this->benefitTypes;
    }

    /**
     * Add skills
     *
     * @param \Highr\HighrBundle\Entity\Skill $skills
     * @return Candidate
     */
    public function addSkill(\Highr\HighrBundle\Entity\Skill $skills)
    {
        $this->skills[] = $skills;

        return $this;
    }

    /**
     * Remove skills
     *
     * @param \Highr\HighrBundle\Entity\Skill $skills
     */
    public function removeSkill(\Highr\HighrBundle\Entity\Skill $skills)
    {
        $this->skills->removeElement($skills);
    }

    /**
     * Get skills
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSkills()
    {
        return $this->skills;
    }

    /**
     * Set address
     *
     * @param \Highr\HighrBundle\Entity\Address $address
     * @return Candidate
     */
    public function setAddress(\Highr\HighrBundle\Entity\Address $address = null)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return \Highr\HighrBundle\Entity\Address 
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Add previousJobs
     *
     * @param \Highr\HighrBundle\Entity\PreviousJob $previousJobs
     * @return Candidate
     */
    public function addPreviousJob(\Highr\HighrBundle\Entity\PreviousJob $previousJobs)
    {
        $this->previousJobs[] = $previousJobs;

        return $this;
    }

    /**
     * Remove previousJobs
     *
     * @param \Highr\HighrBundle\Entity\PreviousJob $previousJobs
     */
    public function removePreviousJob(\Highr\HighrBundle\Entity\PreviousJob $previousJobs)
    {
        $this->previousJobs->removeElement($previousJobs);
    }

    /**
     * Get previousJobs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPreviousJobs()
    {
        return $this->previousJobs;
    }

    /**
     * Set socialProfiles
     *
     * @param \Highr\HighrBundle\Entity\SocialProfile $socialProfiles
     * @return Candidate
     */
    public function setSocialProfiles(\Highr\HighrBundle\Entity\SocialProfile $socialProfiles = null)
    {
        $this->socialProfiles = $socialProfiles;

        return $this;
    }

    /**
     * Get socialProfiles
     *
     * @return \Highr\HighrBundle\Entity\SocialProfile 
     */
    public function getSocialProfiles()
    {
        return $this->socialProfiles;
    }
}
