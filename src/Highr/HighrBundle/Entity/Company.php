<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Company
 *
 * @ORM\Table(name="company")
 * @ORM\Entity(repositoryClass="Highr\HighrBundle\Entity\CompanyRepository")
 */
class Company
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var UploadedFile
     *
     * @ORM\Column(name="image", type="string", length=255, nullable=true)
     */
    private $image;

    /**
     * @var UploadedFile
     *
     * @ORM\Column(name="logo", type="string", length=255, nullable=true)
     */
    private $logo;

    /**
     * @var integer
     *
     * @ORM\Column(name="headcount", type="integer")
     */
    private $headcount;

    /**
     * @var integer
     *
     * @ORM\Column(name="headcount_it", type="integer")
     */
    private $headcountIt;

    /**
     * @var float
     *
     * @ORM\Column(name="kununu_points", type="float")
     */
    private $kununuPoints;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var string
     *
     * @ORM\Column(name="job_elevator_text", type="text")
     */
    private $jobElevatorText;

    /**
     * @var integer
     *
     * @ORM\Column(name="average_age", type="integer")
     */
    private $averageAge;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disabled", type="boolean")
     */
    private $disabled;

    /**
     * @ORM\ManyToMany(targetEntity="CompanyCategory")
     * @ORM\JoinTable(name="companies_companycategories",
     *      joinColumns={@ORM\JoinColumn(name="company_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="companycategory_id", referencedColumnName="id")}
     *      )
     **/
    private $companyCategories;

    /**
     * @ORM\ManyToMany(targetEntity="BenefitType", cascade={"persist"})
     * @ORM\JoinTable(name="companies_benefittypes",
     *      joinColumns={@ORM\JoinColumn(name="company_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="benefittype_id", referencedColumnName="id", unique=true)}
     *      )
     **/
    private $benefitTypes;

    /**
     * @ORM\OneToMany(targetEntity="Job", mappedBy="company")
     **/
    private $jobs;

    /**
     * @ORM\ManyToMany(targetEntity="SkillType")
     * @ORM\JoinTable(name="companies_skilltypes",
     *      joinColumns={@ORM\JoinColumn(name="company_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="skilltype_id", referencedColumnName="id")}
     *      )
     **/
    private $skillTypes;

    /**
     * @ORM\ManyToMany(targetEntity="SocialProfile", cascade={"persist"})
     * @ORM\JoinTable(name="companies_socialprofiles",
     *      joinColumns={@ORM\JoinColumn(name="company_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="socialprofile_id", referencedColumnName="id", unique=true)}
     *      )
     **/
    private $socialProfiles;

    /**
     * @ORM\ManyToMany(targetEntity="CompanyAddress", cascade={"persist"})
     * @ORM\JoinTable(name="companies_companyaddresses",
     *      joinColumns={@ORM\JoinColumn(name="company_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="companyaddress_id", referencedColumnName="id", unique=true)}
     *      )
     **/
    private $companyAddresses;

    public function __construct() {
        $this->benefitTypes = new ArrayCollection();
        $this->companyCategories = new ArrayCollection();
        $this->jobs = new ArrayCollection();
        $this->skillTypes = new ArrayCollection();
        $this->socialProfiles = new ArrayCollection();
        $this->companyAddresses = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Company
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set image
     *
     * @param UploadedFile $image
     * @return Company
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return UploadedFile
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set logo
     *
     * @param UploadedFile $logo
     * @return Company
     */
    public function setLogo($logo)
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * Get logo
     *
     * @return UploadedFile
     */
    public function getLogo()
    {
        return $this->logo;
    }

    /**
     * Set headcount
     *
     * @param integer $headcount
     * @return Company
     */
    public function setHeadcount($headcount)
    {
        $this->headcount = $headcount;

        return $this;
    }

    /**
     * Get headcount
     *
     * @return integer 
     */
    public function getHeadcount()
    {
        return $this->headcount;
    }

    /**
     * Set headcountIt
     *
     * @param integer $headcountIt
     * @return Company
     */
    public function setHeadcountIt($headcountIt)
    {
        $this->headcountIt = $headcountIt;

        return $this;
    }

    /**
     * Get headcountIt
     *
     * @return integer 
     */
    public function getHeadcountIt()
    {
        return $this->headcountIt;
    }

    /**
     * Set kununuPoints
     *
     * @param float $kununuPoints
     * @return Company
     */
    public function setKununuPoints($kununuPoints)
    {
        $this->kununuPoints = $kununuPoints;

        return $this;
    }

    /**
     * Get kununuPoints
     *
     * @return float 
     */
    public function getKununuPoints()
    {
        return $this->kununuPoints;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Company
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return Company
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string 
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set jobElevatorText
     *
     * @param string $jobElevatorText
     * @return Company
     */
    public function setJobElevatorText($jobElevatorText)
    {
        $this->jobElevatorText = $jobElevatorText;

        return $this;
    }

    /**
     * Get jobElevatorText
     *
     * @return string 
     */
    public function getJobElevatorText()
    {
        return $this->jobElevatorText;
    }

    /**
     * Set averageAge
     *
     * @param integer $averageAge
     * @return Company
     */
    public function setAverageAge($averageAge)
    {
        $this->averageAge = $averageAge;

        return $this;
    }

    /**
     * Get averageAge
     *
     * @return integer 
     */
    public function getAverageAge()
    {
        return $this->averageAge;
    }

    /**
     * Set disabled
     *
     * @param boolean $disabled
     * @return Company
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Get disabled
     *
     * @return boolean 
     */
    public function getDisabled()
    {
        return $this->disabled;
    }


    /**
     * Add benefitTypes
     *
     * @param \Highr\HighrBundle\Entity\BenefitType $benefitTypes
     * @return Company
     */
    public function addBenefit(\Highr\HighrBundle\Entity\BenefitType $benefitTypes)
    {
        $this->benefitTypes[] = $benefitTypes;

        return $this;
    }

    public function removeBenefitType(BenefitType $benefitType)
    {
        $this->benefitTypes->removeElement($benefitType);
    }

    /**
     * Get benefitTypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getBenefitTypes()
    {
        return $this->benefitTypes;
    }

    /**
     * Add jobs
     *
     * @param \Highr\HighrBundle\Entity\Job $jobs
     * @return Company
     */
    public function addJob(\Highr\HighrBundle\Entity\Job $jobs)
    {
        $this->jobs[] = $jobs;

        return $this;
    }

    /**
     * Remove jobs
     *
     * @param \Highr\HighrBundle\Entity\Job $jobs
     */
    public function removeJob(\Highr\HighrBundle\Entity\Job $jobs)
    {
        $this->jobs->removeElement($jobs);
    }

    /**
     * Get jobs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobs()
    {
        return $this->jobs;
    }

    /**
     * Add skillTypes
     *
     * @param \Highr\HighrBundle\Entity\SkillType $skillTypes
     * @return Company
     */
    public function addSkillType(\Highr\HighrBundle\Entity\SkillType $skillTypes)
    {
        $this->skillTypes[] = $skillTypes;

        return $this;
    }

    /**
     * Remove skillTypes
     *
     * @param \Highr\HighrBundle\Entity\SkillType $skillTypes
     */
    public function removeSkillType(\Highr\HighrBundle\Entity\SkillType $skillTypes)
    {
        $this->skillTypes->removeElement($skillTypes);
    }

    /**
     * Get skillTypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSkillTypes()
    {
        return $this->skillTypes;
    }

    /**
     * Add socialProfiles
     *
     * @param \Highr\HighrBundle\Entity\SocialProfile $socialProfiles
     * @return Company
     */
    public function addSocialProfile(\Highr\HighrBundle\Entity\SocialProfile $socialProfiles)
    {
        $this->socialProfiles[] = $socialProfiles;

        return $this;
    }

    /**
     * Remove socialProfiles
     *
     * @param \Highr\HighrBundle\Entity\SocialProfile $socialProfiles
     */
    public function removeSocialProfile(\Highr\HighrBundle\Entity\SocialProfile $socialProfiles)
    {
        $this->socialProfiles->removeElement($socialProfiles);
    }

    /**
     * Get socialProfiles
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSocialProfiles()
    {
        return $this->socialProfiles;
    }

    /**
     * Add companyAddresses
     *
     * @param \Highr\HighrBundle\Entity\CompanyAddress $companyAddresses
     * @return Company
     */
    public function addCompanyAddress(\Highr\HighrBundle\Entity\CompanyAddress $companyAddresses)
    {
        $this->companyAddresses[] = $companyAddresses;

        return $this;
    }

    /**
     * Remove companyAddresses
     *
     * @param \Highr\HighrBundle\Entity\CompanyAddress $companyAddresses
     */
    public function removeCompanyAddress(\Highr\HighrBundle\Entity\CompanyAddress $companyAddresses)
    {
        $this->companyAddresses->removeElement($companyAddresses);
    }

    /**
     * Get companyAddresses
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCompanyAddresses()
    {
        return $this->companyAddresses;
    }

    /**
     * Add companyCategories
     *
     * @param \Highr\HighrBundle\Entity\CompanyCategory $companyCategories
     * @return Company
     */
    public function addCompanyCategory(\Highr\HighrBundle\Entity\CompanyCategory $companyCategories)
    {
        $this->companyCategories[] = $companyCategories;

        return $this;
    }

    /**
     * Remove companyCategories
     *
     * @param \Highr\HighrBundle\Entity\CompanyCategory $companyCategories
     */
    public function removeCompanyCategory(\Highr\HighrBundle\Entity\CompanyCategory $companyCategories)
    {
        $this->companyCategories->removeElement($companyCategories);
    }

    /**
     * Get companyCategories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getCompanyCategories()
    {
        return $this->companyCategories;
    }
}
