<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CompanyAddress
 *
 * @ORM\Table(name="company_address")
 * @ORM\Entity
 */
class CompanyAddress extends Address
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_headquarter", type="boolean")
     */
    private $isHeadquarter;

    /**
     * @ORM\OneToOne(targetEntity="ContactPerson")
     * @ORM\JoinColumn(name="contactperson_id", referencedColumnName="id")
     **/
    private $contactPerson;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isHeadquarter
     *
     * @param boolean $isHeadquarter
     * @return CompanyAddress
     */
    public function setIsHeadquarter($isHeadquarter)
    {
        $this->isHeadquarter = $isHeadquarter;

        return $this;
    }

    /**
     * Get isHeadquarter
     *
     * @return boolean 
     */
    public function getIsHeadquarter()
    {
        return $this->isHeadquarter;
    }

    /**
     * Set contactPerson
     *
     * @param \Highr\HighrBundle\Entity\ContactPerson $contactPerson
     * @return CompanyAddress
     */
    public function setContactPerson(\Highr\HighrBundle\Entity\ContactPerson $contactPerson = null)
    {
        $this->contactPerson = $contactPerson;

        return $this;
    }

    /**
     * Get contactPerson
     *
     * @return \Highr\HighrBundle\Entity\ContactPerson 
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }
}
