<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * CompanyCategoryRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class CompanyCategoryRepository extends EntityRepository
{
}
