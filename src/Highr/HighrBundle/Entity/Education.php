<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Education
 *
 * @ORM\Table(name="education")
 * @ORM\Entity
 */
class Education
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="uni", type="string", length=255)
     */
    private $uni;

    /**
     * @var string
     *
     * @ORM\Column(name="field_of_study", type="string", length=255)
     */
    private $fieldOfStudy;

    /**
     * @var string
     *
     * @ORM\Column(name="degree", type="string", length=255)
     */
    private $degree;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="graduation_year", type="datetime")
     */
    private $graduationYear;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set uni
     *
     * @param string $uni
     * @return Education
     */
    public function setUni($uni)
    {
        $this->uni = $uni;

        return $this;
    }

    /**
     * Get uni
     *
     * @return string 
     */
    public function getUni()
    {
        return $this->uni;
    }

    /**
     * Set fieldOfStudy
     *
     * @param string $fieldOfStudy
     * @return Education
     */
    public function setFieldOfStudy($fieldOfStudy)
    {
        $this->fieldOfStudy = $fieldOfStudy;

        return $this;
    }

    /**
     * Get fieldOfStudy
     *
     * @return string 
     */
    public function getFieldOfStudy()
    {
        return $this->fieldOfStudy;
    }

    /**
     * Set degree
     *
     * @param string $degree
     * @return Education
     */
    public function setDegree($degree)
    {
        $this->degree = $degree;

        return $this;
    }

    /**
     * Get degree
     *
     * @return string 
     */
    public function getDegree()
    {
        return $this->degree;
    }

    /**
     * Set graduationYear
     *
     * @param \DateTime $graduationYear
     * @return Education
     */
    public function setGraduationYear($graduationYear)
    {
        $this->graduationYear = $graduationYear;

        return $this;
    }

    /**
     * Get graduationYear
     *
     * @return \DateTime 
     */
    public function getGraduationYear()
    {
        return $this->graduationYear;
    }
}
