<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Job
 *
 * @ORM\Table(name="job")
 * @ORM\Entity
 */
class Job
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Company", inversedBy="jobs")
     * @ORM\JoinColumn(name="job_id", referencedColumnName="id")
     **/
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var integer
     *
     * @ORM\Column(name="desired_salary", type="integer")
     */
    private $desiredSalary;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="entry_date", type="datetime")
     */
    private $entryDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="disabled", type="boolean")
     */
    private $disabled;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="reference_number", type="string", length=255)
     */
    private $referenceNumber;

    /**
     * @var integer
     *
     * @ORM\Column(name="vacation_days", type="integer")
     */
    private $vacationDays;

    /**
     * @ORM\ManyToOne(targetEntity="JobLevel")
     * @ORM\JoinColumn(name="joblevel_id", referencedColumnName="id")
     **/
    private $jobLevel;

    /**
     * @ORM\OneToOne(targetEntity="CompanyAddress")
     * @ORM\JoinColumn(name="companyaddress_id", referencedColumnName="id")
     **/
    private $companyAddress;

    /**
     * @ORM\ManyToMany(targetEntity="JobSkill")
     * @ORM\JoinTable(name="jobs_jobskills",
     *      joinColumns={@ORM\JoinColumn(name="job_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="jobskill_id", referencedColumnName="id", unique=true)}
     *      )
     **/
    private $jobSkills;

    public function __construct() {
        $this->jobSkills = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Job
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set desiredSalary
     *
     * @param integer $desiredSalary
     * @return Job
     */
    public function setDesiredSalary($desiredSalary)
    {
        $this->desiredSalary = $desiredSalary;

        return $this;
    }

    /**
     * Get desiredSalary
     *
     * @return integer 
     */
    public function getDesiredSalary()
    {
        return $this->desiredSalary;
    }

    /**
     * Set entryDate
     *
     * @param \DateTime $entryDate
     * @return Job
     */
    public function setEntryDate($entryDate)
    {
        $this->entryDate = $entryDate;

        return $this;
    }

    /**
     * Get entryDate
     *
     * @return \DateTime 
     */
    public function getEntryDate()
    {
        return $this->entryDate;
    }

    /**
     * Set disabled
     *
     * @param boolean $disabled
     * @return Job
     */
    public function setDisabled($disabled)
    {
        $this->disabled = $disabled;

        return $this;
    }

    /**
     * Get disabled
     *
     * @return boolean 
     */
    public function getDisabled()
    {
        return $this->disabled;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Job
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set referenceNumber
     *
     * @param string $referenceNumber
     * @return Job
     */
    public function setReferenceNumber($referenceNumber)
    {
        $this->referenceNumber = $referenceNumber;

        return $this;
    }

    /**
     * Get referenceNumber
     *
     * @return string 
     */
    public function getReferenceNumber()
    {
        return $this->referenceNumber;
    }

    /**
     * Set vacationDays
     *
     * @param integer $vacationDays
     * @return Job
     */
    public function setVacationDays($vacationDays)
    {
        $this->vacationDays = $vacationDays;

        return $this;
    }

    /**
     * Get vacationDays
     *
     * @return integer 
     */
    public function getVacationDays()
    {
        return $this->vacationDays;
    }

    /**
     * Set company
     *
     * @param \Highr\HighrBundle\Entity\Company $company
     * @return Job
     */
    public function setCompany(\Highr\HighrBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Highr\HighrBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set jobLevel
     *
     * @param \Highr\HighrBundle\Entity\JobLevel $jobLevel
     * @return Job
     */
    public function setJobLevel(\Highr\HighrBundle\Entity\JobLevel $jobLevel = null)
    {
        $this->jobLevel = $jobLevel;

        return $this;
    }

    /**
     * Get jobLevel
     *
     * @return \Highr\HighrBundle\Entity\JobLevel 
     */
    public function getJobLevel()
    {
        return $this->jobLevel;
    }

    /**
     * Set companyAddress
     *
     * @param \Highr\HighrBundle\Entity\CompanyAddress $companyAddress
     * @return Job
     */
    public function setCompanyAddress(\Highr\HighrBundle\Entity\CompanyAddress $companyAddress = null)
    {
        $this->companyAddress = $companyAddress;

        return $this;
    }

    /**
     * Get companyAddress
     *
     * @return \Highr\HighrBundle\Entity\CompanyAddress 
     */
    public function getCompanyAddress()
    {
        return $this->companyAddress;
    }

    /**
     * Add jobSkills
     *
     * @param \Highr\HighrBundle\Entity\JobSkill $jobSkills
     * @return Job
     */
    public function addJobSkill(\Highr\HighrBundle\Entity\JobSkill $jobSkills)
    {
        $this->jobSkills[] = $jobSkills;

        return $this;
    }

    /**
     * Remove jobSkills
     *
     * @param \Highr\HighrBundle\Entity\JobSkill $jobSkills
     */
    public function removeJobSkill(\Highr\HighrBundle\Entity\JobSkill $jobSkills)
    {
        $this->jobSkills->removeElement($jobSkills);
    }

    /**
     * Get jobSkills
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getJobSkills()
    {
        return $this->jobSkills;
    }
}
