<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JobRequest
 *
 * @ORM\Table(name="job_request")
 * @ORM\Entity
 */
class JobRequest
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer")
     */
    private $state;

    /**
     * @var boolean
     *
     * @ORM\Column(name="company_requests_candidate", type="boolean")
     */
    private $companyRequestsCandidate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="candidate_requests_company", type="boolean")
     */
    private $candidateRequestsCompany;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return JobRequest
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set companyRequestsCandidate
     *
     * @param boolean $companyRequestsCandidate
     * @return JobRequest
     */
    public function setCompanyRequestsCandidate($companyRequestsCandidate)
    {
        $this->companyRequestsCandidate = $companyRequestsCandidate;

        return $this;
    }

    /**
     * Get companyRequestsCandidate
     *
     * @return boolean 
     */
    public function getCompanyRequestsCandidate()
    {
        return $this->companyRequestsCandidate;
    }

    /**
     * Set candidateRequestsCompany
     *
     * @param boolean $candidateRequestsCompany
     * @return JobRequest
     */
    public function setCandidateRequestsCompany($candidateRequestsCompany)
    {
        $this->candidateRequestsCompany = $candidateRequestsCompany;

        return $this;
    }

    /**
     * Get candidateRequestsCompany
     *
     * @return boolean 
     */
    public function getCandidateRequestsCompany()
    {
        return $this->candidateRequestsCompany;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return JobRequest
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set message
     *
     * @param string $message
     * @return JobRequest
     */
    public function setMessage($message)
    {
        $this->message = $message;

        return $this;
    }

    /**
     * Get message
     *
     * @return string 
     */
    public function getMessage()
    {
        return $this->message;
    }
}
