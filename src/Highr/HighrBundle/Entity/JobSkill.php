<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * JobSkill
 *
 * @ORM\Table(name="job_skill")
 * @ORM\Entity
 */
class JobSkill
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_must_have", type="boolean")
     */
    private $isMustHave;

    /**
     * @ORM\ManyToOne(targetEntity="SkillType")
     * @ORM\JoinColumn(name="skilltype_id", referencedColumnName="id")
     **/
    private $skillType;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set isMustHave
     *
     * @param boolean $isMustHave
     * @return JobSkill
     */
    public function setIsMustHave($isMustHave)
    {
        $this->isMustHave = $isMustHave;

        return $this;
    }

    /**
     * Get isMustHave
     *
     * @return boolean 
     */
    public function getIsMustHave()
    {
        return $this->isMustHave;
    }

    /**
     * Set skillType
     *
     * @param \Highr\HighrBundle\Entity\SkillType $skillType
     * @return JobSkill
     */
    public function setSkillType(\Highr\HighrBundle\Entity\SkillType $skillType = null)
    {
        $this->skillType = $skillType;

        return $this;
    }

    /**
     * Get skillType
     *
     * @return \Highr\HighrBundle\Entity\SkillType 
     */
    public function getSkillType()
    {
        return $this->skillType;
    }
}
