<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PreviousJob
 *
 * @ORM\Table(name="previous_job")
 * @ORM\Entity
 */
class PreviousJob
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="from_date", type="datetime")
     */
    private $fromDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="to_date", type="datetime")
     */
    private $toDate;

    /**
     * @var string
     *
     * @ORM\Column(name="position", type="string", length=255)
     */
    private $position;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_freelancer", type="boolean")
     */
    private $isFreelancer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_permanent_worker", type="boolean")
     */
    private $isPermanentWorker;

    /**
     * @var string
     *
     * @ORM\Column(name="company_name", type="string", length=255)
     */
    private $companyName;

    /**
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     **/
    private $company;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fromDate
     *
     * @param \DateTime $fromDate
     * @return PreviousJob
     */
    public function setFromDate($fromDate)
    {
        $this->fromDate = $fromDate;

        return $this;
    }

    /**
     * Get fromDate
     *
     * @return \DateTime 
     */
    public function getFromDate()
    {
        return $this->fromDate;
    }

    /**
     * Set toDate
     *
     * @param \DateTime $toDate
     * @return PreviousJob
     */
    public function setToDate($toDate)
    {
        $this->toDate = $toDate;

        return $this;
    }

    /**
     * Get toDate
     *
     * @return \DateTime 
     */
    public function getToDate()
    {
        return $this->toDate;
    }

    /**
     * Set position
     *
     * @param string $position
     * @return PreviousJob
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string 
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return PreviousJob
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set isFreelancer
     *
     * @param boolean $isFreelancer
     * @return PreviousJob
     */
    public function setIsFreelancer($isFreelancer)
    {
        $this->isFreelancer = $isFreelancer;

        return $this;
    }

    /**
     * Get isFreelancer
     *
     * @return boolean 
     */
    public function getIsFreelancer()
    {
        return $this->isFreelancer;
    }

    /**
     * Set isPermanentWorker
     *
     * @param boolean $isPermanentWorker
     * @return PreviousJob
     */
    public function setIsPermanentWorker($isPermanentWorker)
    {
        $this->isPermanentWorker = $isPermanentWorker;

        return $this;
    }

    /**
     * Get isPermanentWorker
     *
     * @return boolean 
     */
    public function getIsPermanentWorker()
    {
        return $this->isPermanentWorker;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     * @return PreviousJob
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set company
     *
     * @param \Highr\HighrBundle\Entity\Company $company
     * @return PreviousJob
     */
    public function setCompany(\Highr\HighrBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Highr\HighrBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }
}
