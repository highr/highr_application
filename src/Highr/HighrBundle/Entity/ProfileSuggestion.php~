<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ProfileSuggestion
 *
 * @ORM\Table(name="profile_suggestion")
 * @ORM\Entity
 */
class ProfileSuggestion
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var boolean
     *
     * @ORM\Column(name="company_to_candidate", type="boolean")
     */
    private $companyToCandidate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="candidate_to_company", type="boolean")
     */
    private $candidateToCompany;

    /**
     * @ORM\ManyToOne(targetEntity="Candidate")
     * @ORM\JoinColumn(name="candidate_id", referencedColumnName="id")
     **/
    private $candidate;

    /**
     * @ORM\ManyToOne(targetEntity="Company")
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     **/
    private $company;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return ProfileSuggestion
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime 
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set companyToCandidate
     *
     * @param boolean $companyToCandidate
     * @return ProfileSuggestion
     */
    public function setCompanyToCandidate($companyToCandidate)
    {
        $this->companyToCandidate = $companyToCandidate;

        return $this;
    }

    /**
     * Get companyToCandidate
     *
     * @return boolean 
     */
    public function getCompanyToCandidate()
    {
        return $this->companyToCandidate;
    }

    /**
     * Set candidateToCompany
     *
     * @param boolean $candidateToCompany
     * @return ProfileSuggestion
     */
    public function setCandidateToCompany($candidateToCompany)
    {
        $this->candidateToCompany = $candidateToCompany;

        return $this;
    }

    /**
     * Get candidateToCompany
     *
     * @return boolean 
     */
    public function getCandidateToCompany()
    {
        return $this->candidateToCompany;
    }

    /**
     * Set candidate
     *
     * @param \Highr\HighrBundle\Entity\Candidate $candidate
     * @return ProfileSuggestion
     */
    public function setCandidate(\Highr\HighrBundle\Entity\Candidate $candidate = null)
    {
        $this->candidate = $candidate;

        return $this;
    }

    /**
     * Get candidate
     *
     * @return \Highr\HighrBundle\Entity\Candidate 
     */
    public function getCandidate()
    {
        return $this->candidate;
    }

    /**
     * Set company
     *
     * @param \Highr\HighrBundle\Entity\Company $company
     * @return ProfileSuggestion
     */
    public function setCompany(\Highr\HighrBundle\Entity\Company $company = null)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return \Highr\HighrBundle\Entity\Company 
     */
    public function getCompany()
    {
        return $this->company;
    }
}
