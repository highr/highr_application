<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Reference
 *
 * @ORM\Table(name="reference")
 * @ORM\Entity
 */
class Reference
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=255)
     */
    private $website;

    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=255)
     */
    private $role;

    /**
     * @var string
     *
     * @ORM\Column(name="function_of_project", type="string", length=255)
     */
    private $functionOfProject;

    /**
     * @ORM\ManyToMany(targetEntity="SkillType")
     * @ORM\JoinTable(name="references_skilltypes",
     *      joinColumns={@ORM\JoinColumn(name="reference_id", referencedColumnName="id")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="skilltype_id", referencedColumnName="id", unique=true)}
     *      )
     **/
    private $skillTypes;

    public function __construct() {
        $this->skillTypes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Reference
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set website
     *
     * @param string $website
     * @return Reference
     */
    public function setWebsite($website)
    {
        $this->website = $website;

        return $this;
    }

    /**
     * Get website
     *
     * @return string 
     */
    public function getWebsite()
    {
        return $this->website;
    }

    /**
     * Set role
     *
     * @param string $role
     * @return Reference
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string 
     */
    public function getRole()
    {
        return $this->role;
    }

    /**
     * Set functionOfProject
     *
     * @param string $functionOfProject
     * @return Reference
     */
    public function setFunctionOfProject($functionOfProject)
    {
        $this->functionOfProject = $functionOfProject;

        return $this;
    }

    /**
     * Get functionOfProject
     *
     * @return string 
     */
    public function getFunctionOfProject()
    {
        return $this->functionOfProject;
    }

    /**
     * Add skillTypes
     *
     * @param \Highr\HighrBundle\Entity\SkillType $skillTypes
     * @return Reference
     */
    public function addSkillType(\Highr\HighrBundle\Entity\SkillType $skillTypes)
    {
        $this->skillTypes[] = $skillTypes;

        return $this;
    }

    /**
     * Remove skillTypes
     *
     * @param \Highr\HighrBundle\Entity\SkillType $skillTypes
     */
    public function removeSkillType(\Highr\HighrBundle\Entity\SkillType $skillTypes)
    {
        $this->skillTypes->removeElement($skillTypes);
    }

    /**
     * Get skillTypes
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSkillTypes()
    {
        return $this->skillTypes;
    }
}
