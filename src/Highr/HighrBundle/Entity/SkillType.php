<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SkillType
 *
 * @ORM\Table(name="skill_type")
 * @ORM\Entity
 */
class SkillType
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="SkillGroup")
     * @ORM\JoinColumn(name="skillgroup_id", referencedColumnName="id")
     **/
    private $skillGroup;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return SkillType
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set skillGroup
     *
     * @param \Highr\HighrBundle\Entity\SkillGroup $skillGroup
     * @return SkillType
     */
    public function setSkillGroup(\Highr\HighrBundle\Entity\SkillGroup $skillGroup = null)
    {
        $this->skillGroup = $skillGroup;

        return $this;
    }

    /**
     * Get skillGroup
     *
     * @return \Highr\HighrBundle\Entity\SkillGroup 
     */
    public function getSkillGroup()
    {
        return $this->skillGroup;
    }
}
