<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SocialProfile
 *
 * @ORM\Table(name="social_profile")
 * @ORM\Entity
 */
class SocialProfile
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="link", type="string", length=255)
     */
    private $link;

    /**
     * @ORM\ManyToOne(targetEntity="SocialProfileType")
     * @ORM\JoinColumn(name="socialprofiletype_id", referencedColumnName="id")
     **/
    private $socialProfileType;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set link
     *
     * @param string $link
     * @return SocialProfile
     */
    public function setLink($link)
    {
        $this->link = $link;

        return $this;
    }

    /**
     * Get link
     *
     * @return string 
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * Set socialProfileType
     *
     * @param \Highr\HighrBundle\Entity\SocialProfileType $socialProfileType
     * @return SocialProfile
     */
    public function setSocialProfileType(\Highr\HighrBundle\Entity\SocialProfileType $socialProfileType = null)
    {
        $this->socialProfileType = $socialProfileType;

        return $this;
    }

    /**
     * Get socialProfileType
     *
     * @return \Highr\HighrBundle\Entity\SocialProfileType 
     */
    public function getSocialProfileType()
    {
        return $this->socialProfileType;
    }
}
