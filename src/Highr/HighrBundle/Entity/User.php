<?php

namespace Highr\HighrBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

/**
 * User
 *
 * @ORM\Table(name="highr_user")
 * @ORM\Entity
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255)
     */
    private $lastName;

    /**
     * @var integer
     *
     * @ORM\Column(name="state", type="integer")
     */
    private $state;

    /**
     * @var Candidate
     *
     * @ORM\OneToOne(targetEntity="Candidate", cascade={"persist"})
     * @ORM\JoinColumn(name="candidate_id", referencedColumnName="id")
     */
    private $candidate;

    /**
     * @var Company
     *
     * @ORM\OneToOne(targetEntity="Company", cascade={"persist"})
     * @ORM\JoinColumn(name="company_id", referencedColumnName="id")
     */
    private $company;

    public function __construct() {
        parent::__construct();
        $this->addRole('ROLE_CANDIDATE');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string 
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string 
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set state
     *
     * @param integer $state
     * @return User
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return integer 
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return Candidate
     */
    public function getCandidate() {
        return $this->candidate;
    }

    /**
     * @param Candidate $candidate
     */
    public function setCandidate($candidate = null) {
        $this->candidate = $candidate;
    }

    /**
     * @return Company
     */
    public function getCompany() {
        return $this->company;
    }

    /**
     * @param Company $company
     */
    public function setCompany($company = null) {
        $this->company = $company;
    }

}
