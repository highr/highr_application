<?php

namespace Highr\HighrBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class BenefitType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('benefitType', 'entity', array(
                'class' => 'HighrBundle:BenefitType',
                'property' => 'title'
            ))
            ->add('description')
            
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Highr\HighrBundle\Entity\Benefit'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'highr_highrbundle_benefit';
    }
}
