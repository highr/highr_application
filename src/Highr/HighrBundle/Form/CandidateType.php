<?php

namespace Highr\HighrBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CandidateType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('birthday')
            ->add('nationality')
            ->add('aboutMe')
            ->add('image')
            ->add('currentSalary')
            ->add('desiredSalary')
            ->add('minimumSalary')
            ->add('motivation')
            ->add('cancelationPeriod')
            ->add('desiredTeamSize')
            ->add('sex')
            ->add('desiredNextLevel')
            ->add('educations')
            ->add('benefitTypes','entity', array(
                'class' => 'HighrBundle:BenefitType',
                'property' => 'title'
    ))
            ->add('skills')
            ->add('address')
            ->add('previousJobs')
            ->add('socialProfiles')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Highr\HighrBundle\Entity\Candidate'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'highr_highrbundle_candidate';
    }
}
