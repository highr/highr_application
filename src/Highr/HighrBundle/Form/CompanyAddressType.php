<?php

namespace Highr\HighrBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Doctrine\ORM\EntityManager;

class CompanyAddressType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder
            ->add('isHeadquarter', 'checkbox', array(
                'label' => 'Hauptsitz',
                'required' => false
            ))
            ->add('street', 'text', array(
                'attr' => array('class' => 'form-control'),
                'required' => true
            ))
            ->add('houseNumber', 'text', array(
                'attr' => array('class' => 'form-control'),
                'required' => true
            ))
            ->add('zip', 'text', array(
                'attr' => array('class' => 'form-control'),
                'required' => true
            ))
            ->add('city', 'text', array(
                'attr' => array('class' => 'form-control'),
                'required' => true
            ))
            ->add('country', 'text', array(
                'attr' => array('class' => 'form-control'),
                'required' => true
            ))
            ->add('telephone', 'text', array(
                'attr' => array('class' => 'form-control'),
                'required' => true
            ))
            ->add('mobile', 'text', array(
                'attr' => array('class' => 'form-control'),
                'required' => false
            ))
            ->add('email', 'text', array(
                'attr' => array('class' => 'form-control'),
                'required' => true
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Highr\HighrBundle\Entity\CompanyAddress'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'highr_highrbundle_companyaddress';
    }
}
