<?php

namespace Highr\HighrBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Highr\HighrBundle\Entity\SkillTypeRepository;
use Doctrine\ORM\EntityManager;

class CompanyType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        
        $builder
            ->add('title', 'text', array(
                'label' => 'Name',
                'attr' => array('class' => 'form-control'))
            )
            ->add('image', 'file', array(
                'label' => false,
                'required' => false,
                'data_class' => null
            ))
            ->add('logo', 'file', array(
                'label' => false,
                'required' => false,
                'data_class' => null
            ))
            ->add('headcount', 'integer', array(
                'label' => 'Anzahl der Mitarbeiter'
            ))
            ->add('headcountIt', 'integer', array(
                'label' => 'Anzahl der Mitarbeiter IT',
            ))
            ->add('kununuPoints', 'integer', array(
                'label' => 'Kununu Punkte',
            ))
            ->add('website', 'text', array(
                'label' => 'Webseite',
                'attr' => array('class' => 'form-control'))
            )
            ->add('content', 'textarea', array(
                'label' => 'Beschreibung',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('jobElevatorText', 'textarea', array(
                'label' => 'Job Standard Text',
                'attr' => array(
                    'class' => 'form-control'
                )
            ))
            ->add('averageAge', 'integer', array(
                'label' => 'Durchschnittliches Alter',
            ))
            ->add('companyCategories', 'entity', array(
                'label' => 'Unternehmens Kategorien',
                'class' => 'HighrBundle:CompanyCategory',
                'property' => 'title',
                'expanded' => true,
                'multiple' => true
            ))
            ->add('skillTypes', 'entity', array(
                'label' => 'Skills',
                'class' => 'HighrBundle:SkillType',
                'property' => 'title',
                'expanded' => true,
                'multiple' => true
            ))
            ->add('benefitTypes', 'collection', array(
                'label' => 'Benefittypes',
                'type' => new BenefitType(),
                'allow_add' => true,
                'allow_delete' => true,
            ))
            ->add('socialProfiles', 'collection', array(
                'label' => 'Soziale Netzwerke',
                'type' => new SocialProfileType(),
                'allow_add' => true,
                'allow_delete' => true,
                'prototype_name' => 'SocialProfile'
            ))
            ->add('companyAddresses', 'collection', array(
                'label' => 'Unternehmensadressen',
                'type' => new CompanyAddressType(),
                'allow_add' => true,
                'allow_delete' => true,
                'prototype_name' => 'Adresse'
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Highr\HighrBundle\Entity\Company'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'highr_highrbundle_company';
    }
}
