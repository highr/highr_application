<?php

namespace Highr\HighrBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class ProfileFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('first_name')
            ->add('last_name')
            ->add('state', 'hidden', array(
                'data' => 1
            ))
            ->add('candidate', 'entity', array(
                'class' => 'HighrBundle:Candidate',
                'property' => 'nationality'
            ))
        ;
    }

    public function getParent()
    {
        return 'fos_user_profile';
    }

    public function getName()
    {
        return 'highr_user_profile';
    }
}