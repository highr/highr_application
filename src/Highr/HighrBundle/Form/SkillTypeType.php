<?php

namespace Highr\HighrBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SkillTypeType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('skillGroup', 'entity', array(
                'class' => 'HighrBundle:SkillGroup',
                'property' => 'title'
            ))
            ->add('title', 'text', array(
                'attr' => array('class' => 'form-control'),
                'required' => true
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Highr\HighrBundle\Entity\SkillType'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'highr_highrbundle_skilltype';
    }
}
