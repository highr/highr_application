<?php

namespace Highr\HighrBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class SocialProfileType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('socialProfileType', 'entity', array(
                'class' => 'HighrBundle:SocialProfileType',
                'property' => 'title'
            ))
            ->add('link')   
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Highr\HighrBundle\Entity\SocialProfile'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'highr_highrbundle_socialprofile';
    }
}
